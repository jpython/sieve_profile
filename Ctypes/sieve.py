#!/usr/bin/env python3

import ctypes
import sys,os

#whereami = sys.path[0]
whereami = os.path.dirname(__file__) or sys.path[0]

sieve_c = ctypes.CDLL(whereami + '/libsieve.so').sieve

# void sieve(int number, int *primes);

def sieve(number):
   sieve_c.argtypes = (ctypes.c_int, ctypes.c_int * number)
   sieve_c.restype  = None
   primes = (ctypes.c_int * number)(0)
   sieve_c(number, primes)
   return [i for i in primes if i != 0]

if __name__ == '__main__':
    s = sieve(10000000)
    #print(s)
