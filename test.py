#!/usr/bin/env python3
'''Profile with timeit several Erathostenes' Sieve implementation'''

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import sys

from dotest import do_test


inner =  10
outer =   5
maximum = 10000
logscale = True
display = True

if len(sys.argv) > 1:
    logscale = not ('nolog' in sys.argv[1:])
    display = not ('nodisplay' in sys.argv[1:])
    if 'test' in sys.argv[1:]:
        inner = 10 
        outer = 5
        maximum = 100


# TODO: use argparse
params_args = [ v for v in sys.argv[1:] if v.startswith('params=') ]
if params_args:
    params, = params_args
    params = params.split('=')[1].split(',')
    maximum, inner, outer = [ int(v) for v in params ]

print('maximum, inner, outer, loops:', maximum, inner, outer, inner * outer)

import Ctypes.sieve
import Cmod.sieve
import Cython_cythonized.sieve
import Cython_unmodified.sieve
import Python1.sieve
import Python2.sieve

cases   = 'Python1 Ctypes Cmod Cython_unmodified Cython_cythonized Python2'.split()
legends = [ 'Python\nnaive', 'C library\n(ctypes)',
            'C Python\nModule',
            'Cython\nunmodified', 'Cython\ncythonized',
            'Python\nclever' ]

direct_calls = dict( zip(cases,
        [ Ctypes.sieve.sieve, Cmod.sieve.sieve,
          Cython_cythonized.sieve.sieve, Cython_unmodified.sieve.sieve,
          Python1.sieve.sieve, Python2.sieve.sieve ] ) )

print('Checking all functions doing same stuff')
all = []
for case,sieve in direct_calls.items():
    print(case,':',end='',flush=True) 
    res = frozenset(sieve(100))
    print(res)
    all.append(res)

print('OK!' if len(set(all)) == 1 else ('NOT OK!!!!!'))
print('\n\n Going on...')

names, data = [], []
for case in cases:
    elaps = do_test(case,maximum,inner,outer)
    names.append(case)
    data.append(elaps) 

# Test with pypy3 if available

from subprocess import Popen,PIPE

try:
    with Popen(['pypy3', '-V'],stdout=PIPE) as p:
        for case in 'Python1 Python2'.split():
            with Popen(['pypy3','dotest.py',str(case), \
                    str(maximum),str(inner),str(outer)],stdout=PIPE) as p:
                res = float(p.stdout.read().decode('ASCII'))
            names.append(case)
            data.append(res)
            try:
                legends.append(legends[cases.index(case)] + '\n(pypy3)')
            except ValueError as e:
                sys.stderr.write('WARN: pypy case not found{}\n'.format(case))
except FileNotFoundError:
    sys.stderr.write("pypy3 not available. Skiping a few tests...\n")

legends = [ t + '\n(CPython 3)' if 'pypy3' not in t \
            else t for t in legends ]

from decimal import Decimal
worse = max(data)
data = [ v*100/worse for v in data ]

def autolabels(rects):
    '''Add numeric lables on top of bars.'''
    for rect in rects:
        h = rect.get_height()
        ax.annotate('{:3.2f}'.format(h),
                xy=(rect.get_x() + rect.get_width() / 2, h),
                xytext=(0, 3),  # 3 points vertical offset
                textcoords="offset points",
                ha='center', va='bottom')

matplotlib.rcParams.update({ 'font.size':18 })
matplotlib.rcParams.update({ 'figure.figsize':[19,10] })

width = 0.35
fig, ax = plt.subplots()
x = np.arange(len(names))
ax.set_ylabel('Elapsed time{}'.format(' (log)' if logscale else ''))
ax.set_title("Compare various Eratosthenes' Sieve Python Modules\n" \
             "integer range: {}, inner loops: {}, outer loops: {}".format(maximum,inner,outer)
        )
ax.set_xticks(x)
ax.set_xticklabels(legends,rotation=0)
r = ax.bar(x, data,log=logscale, label= "log(relative time)" if logscale else \
                                        "relative time")
autolabels(r)
ax.legend()
plt.savefig('sieve_profile' if logscale else 'sieve_profile_nolog.png')
if display:
    plt.show()

