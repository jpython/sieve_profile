#!/usr/bin/env python3

def sieve(maximum):
    a = set(range(3, maximum+1, 2)) 
    a |= set([2]) # same as "a += b" for sets, ie a union.
    p = 3
    while p < maximum ** 0.5:
        a -= set(range(p * p, maximum+1, 2 * p))
        p += 2
        while p not in a:
            p += 2
    return a
    
if __name__ == '__main__':
    print(sieve(10000000))
