#include <stdio.h>
#include <stdlib.h>

#include "libsieve.h"

int main() {
    int number,*primes;

    printf("Enter the max number: ");
    scanf("%d",&number);
    primes = (int *)malloc(number * sizeof(int));
    sieve(number,primes);
    int i;
    for(i =2; i<=number; i++) {
	    if (primes[i]) {
		    printf("%d ",primes[i]);
            }
    }
    free(primes);
    printf("\n");
    exit(0);
}
