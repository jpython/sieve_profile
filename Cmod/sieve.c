#include <Python.h>
#include "libsieve.h"

// inspired by: https://gist.github.com/physacco/2e1b52415f3a964ad2a542a99bebed8f

static PyObject* sieve_py(PyObject *self, PyObject *args) {
    const int number;
    int *primes,i;

    if (!PyArg_ParseTuple(args, "i", &number)) {
        return NULL;
    }
    primes = (int *)malloc(number * sizeof(int));
    sieve(number,primes);

    PyObject *result = PyList_New(0);

    for(i =2; i<=number; i++) {
        if (primes[i]) {
    	    PyList_Append(result,PyLong_FromLong(primes[i]));
	    //printf("%d ",primes[i]);
        }
    }
    free(primes);
    //printf("Hello, %d!\n", number);
    //Py_RETURN_NONE;
    return result;
}

// Method definition object for this extension, these argumens mean:
// ml_name: The name of the method
// ml_meth: Function pointer to the method implementation
// ml_flags: Flags indicating special features of this method, such as
//          accepting arguments, accepting keyword arguments, being a
//          class method, or being a static method of a class.
// ml_doc:  Contents of this method's docstring
static PyMethodDef sieve_methods[] = { 
    {   
        "sieve", sieve_py, METH_VARARGS,
        "Return primes numbers up to argument"
    },  
    {NULL, NULL, 0, NULL}
};

// Module definition
// The arguments of this structure tell Python what to call your extension,
// what it's methods are and where to look for it's method definitions
static struct PyModuleDef sieve_definition = { 
    PyModuleDef_HEAD_INIT,
    "sieve",
    "A Python module that implement Eratostenes sieve algorithm in C",
    -1, 
    sieve_methods
};

// Module initialization
// Python calls this function when importing your extension. It is important
// that this function is named PyInit_[[your_module_name]] exactly, and matches
// the name keyword argument in setup.py's setup() call.
PyMODINIT_FUNC PyInit_sieve(void) {
    Py_Initialize();
    return PyModule_Create(&sieve_definition);
}

