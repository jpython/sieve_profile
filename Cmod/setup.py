#!/usr/bin/env python3
# encoding: utf-8

from distutils.core import setup, Extension

sieve_module = Extension(
        'sieve', sources = ['sieve.c'],
        include_dirs = ['.'],
        libraries = ['sieve'],
        library_dirs = ['.'],
        )

setup(name='sieve',
      version='0.1.0',
      description='Erathostenes sieve written in C',
      ext_modules=[sieve_module])
