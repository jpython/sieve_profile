binaries:
	$(MAKE) -C Ctypes
	$(MAKE) -C Cmod
	$(MAKE) -C Cython_cythonized
	$(MAKE) -C Cython_unmodified

run: binaries
	./test.py

images:
	./test.py nodisplay
	./test.py nolog nodisplay

test: binaries
	./test.py test

nolog: binaries
	./test.py nolog

test-nolog: binaries
	./test.py  test nolog

clean:
	$(MAKE) -C Ctypes clean
	$(MAKE) -C Cmod clean
	$(MAKE) -C Cython_cythonized clean
	$(MAKE) -C Cython_unmodified clean
	$(MAKE) -C Python1 clean
	$(MAKE) -C Python2 clean
	rm -rf __pycache__
