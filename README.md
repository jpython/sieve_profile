# Sieve_Profile

Profile several Eratosthenes' Sieve in Python (plain, optimized, C, C with ctypes, Cython, pypy)

From a Python 3 virtual environnement install requirements:

`pip install -r requirements.txt`

A suitable C/C++ GCC environment should be available.

Then to run all the profiling tests, run:

`make run`

![](sieve_profile.png)

![](sieve_profile_nolog.png)
