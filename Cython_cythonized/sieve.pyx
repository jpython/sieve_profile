from libcpp.vector cimport vector

def sieve(int maximum):
    cdef int n, i
    cdef vector[int] p
    p.reserve(maximum)  # allocate memory for 'nb_primes' elements.

    n = 2
    while n <= maximum: 
        for i in p:
            if n % i == 0:
                break
        else:
            p.push_back(n)  # push_back is similar to append()
        n += 1

    # Vectors are automatically converted to Python
    # lists when converted to Python objects.
    return p
