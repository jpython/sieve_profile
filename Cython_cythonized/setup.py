from distutils.core import setup,Extension
from Cython.Build import cythonize

ext_modules=[
        Extension("sieve",
            sources  = ["sieve.pyx"],
            language = "c++",
            extra_compile_args = ['-O6'],
        )]

setup(
    name = 'Cythonized Eratostenes sieve algorithm',
    ext_modules = cythonize(ext_modules)
)
