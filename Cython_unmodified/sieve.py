#!/usr/bin/env python3

def sieve(maximum):
    a = [2]
    a += list(range(3, maximum+1, 2))
    for i in range(3, int(maximum**0.5) + 1, 2):
        for j in range(i * i, maximum + 1, i * 2):
            if j in a:
                a.remove(j)
    return a
    

