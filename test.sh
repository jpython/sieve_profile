#!/usr/bin/env bash

cases="Ctypes Cmod  Cython_cythonized  Cython_unmodified  Python1  Python2"

export LD_LIBRARY_PATH=.
for case in $cases
do
  cd $case
  echo $case
  python3 -m timeit 'from sieve import sieve; r = sieve(10000) '
  cd ..
done
