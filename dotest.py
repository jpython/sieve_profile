#!/usr/bin/env python3
'''Compute time for a sieve function in do_test.
As a script wait for case_name maximum inner outer arguments
(for pypy3 tests) and returns time in stdout.'''

from timeit import timeit
import sys

def do_test(case,maximum,inner,outer):
    '''Compute time of execution of sieve(maximum) in an inner loop,
    run the test outer times'''
    sys.stderr.write('{:20}\t'.format(case))
    setup = 'import {}.sieve as this_sieve\n'.format(case)
    elaps = timeit(
               '[ this_sieve.sieve({})\n' \
               '  for i in range({}) ]'.format(maximum,inner),
               setup = setup,
               number = outer 
            )
    sys.stderr.write('{:20.2f}\n'.format(elaps * 10000))
    return elaps

if __name__ == '__main__':
    usage = "Usage: {} case_name maximum inner outer\n".format(sys.argv[0])
    if len(sys.argv) != 5:
        sys.stderr.write(usage)
        exit(1)
    else:
        case = sys.argv[1]
        maximum, inner, outer = [ int(v) for v in sys.argv[2:] ]
        print(do_test(case,maximum,inner,outer))
        exit(0)
